/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labhandson;

/**
 *
 * @author gopikrishnamanchukonda
 */
public class LabHandson {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Pass by value
        Test t = new Test();
        
        t.testMethod(5);
        t.testMethod(0);
        t.testMethod(100);
        System.out.println("");
        //pass by reference 
        Value v = new Value(8);
        Test2 y = new Test2();
        y.test2Method(v);
        
    }
    
    
}
class Test{
        public void testMethod(int m){
            System.out.println("Value of the m is "+m);
        }
    }
class Test2{
    public void test2Method(Value v){
        System.out.println("value of  v is:"+v.getM());
    }
}
class Value
{
    int m;
    Value(int m){
         this.m = m;
    }
    int getM(){
       return m;
       
    }
}